<?php

use App\Common\Money;
use PHPUnit\Framework\TestCase;
use App\Infrastructure\HttpClientInterface;
use App\Providers\Rate\ExchangeRatesApiDataProvider;
use App\Providers\Rate\CurrencyNotFoundException;
use App\Infrastructure\ServerNotAvailableException;
use App\Providers\Rate\BadResponseException;

class ExchangeRatesApiDataProviderTest extends TestCase
{

    public function testFindRateBaseCurrency(): void
    {
        $mock = $this->createMock(HttpClientInterface::class);
        $mock->method('send')
            ->willReturn([
                'rates' => ['GBP' => '0.90328'],
                'base' => 'EUR',
                'date' => '2020-06-22'
            ]);

        $expectedResult = new Money('0.90328', 'EUR');
        $provider = new ExchangeRatesApiDataProvider($mock);
        $result = $provider->findRateToBaseCurrency('GBP');
        $this->assertEquals($result, $expectedResult);

        $mock->method('send')->willThrowException(new ServerNotAvailableException());
        $this->expectException(BadResponseException::class);
        $provider->findRateToBaseCurrency('GBP');

        $mock->method('send')->willReturn(['rates' => ['']]);
        $this->expectException(CurrencyNotFoundException::class);
        $provider->findRateToBaseCurrency('GBP');
    }
}