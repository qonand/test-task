<?php

use App\Common\Money;
use PHPUnit\Framework\TestCase;

class MoneyTest extends TestCase
{
    public function testDivide(): void
    {
        $fiveEuro = new Money('5.0000', 'EUR');
        $tenEuro = new Money('10.0000', 'EUR');

        $result = $tenEuro->divide(2);
        $this->assertEquals($fiveEuro, $result);
    }

    public function testMultiply(): void
    {
        $tenEuro = new Money('10.0000', 'EUR');
        $twentyEuro = new Money('20.0000', 'EUR');

        $result = $tenEuro->multiply(2);
        $this->assertEquals($twentyEuro, $result);
    }

    public function testConvert(): void
    {
        $tenGbr = new Money('10.0000', 'GBP');

        $expectedResult = new Money('11.11111', 'EUR');
        $result = $tenGbr->convertToBaseCurrency('0.9');
        $this->assertEquals($expectedResult, $result);
    }

    public function testCeil(): void
    {
        $money = new Money('11.12345', 'EUR');
        $expectedResult = new Money('11.13', 'EUR');
        $result = $money->ceil(2);
        $this->assertEquals($expectedResult, $result);
    }
}