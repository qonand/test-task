<?php

use App\Common\MathHelper;
use PHPUnit\Framework\TestCase;

class MathHelperTest extends TestCase
{
    public function dataProvider(): array
    {
        return [
            ['100.1234567', 6, '100.123457'],
            ['100.1234567', 5, '100.12346'],
            ['100.1234567', 4, '100.1235'],
            ['100.1234567', 3, '100.124'],
            ['100.1234567', 2, '100.13'],
            ['100.1234567', 1, '100.2'],
            ['100.1234567', 0, '101'],
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testCeil(string $value, int $scale, string $expectedResult): void
    {
        $result = MathHelper::ceil($value, $scale);
        $this->assertEquals($expectedResult, $result);
    }
}