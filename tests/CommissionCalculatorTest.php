<?php

use PHPUnit\Framework\TestCase;
use App\Common\Money;
use App\Providers\Bin\CountryCode;
use App\Calculator\CommissionCalculator;
use App\Providers\Bin\BinDataProviderInterface;
use App\Providers\Rate\RateDataProviderInterface;
use App\Providers\Transaction\Transaction;

class CommissionCalculatorTest extends TestCase
{
    private $binDataProviderMock;

    private $rateDataProviderMock;

    private $calculator;

    protected function setUp(): void
    {
        $this->binDataProviderMock = $this->createMock(BinDataProviderInterface::class);
        $this->rateDataProviderMock = $this->createMock(RateDataProviderInterface::class);
        $this->calculator = new CommissionCalculator($this->binDataProviderMock, $this->rateDataProviderMock);
    }

    protected function invokeMethod($object, string $method, array $params)
    {
        $class = get_class($object);
        $method = new ReflectionMethod($class, $method);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $params);
    }

    public function dataProviderForCalc(): array
    {
        return [
            [
                '45717360',
                new Money('100.00', 'EUR'),
                new CountryCode('DK'),
                Money::createFromBaseCurrency('1'),
                Money::createFromBaseCurrency('1.0000')
            ],
            [
                '516793',
                new Money('50.00', 'USD'),
                new CountryCode('LT'),
                Money::createFromBaseCurrency('1.1213'),
                Money::createFromBaseCurrency('0.44591')
            ],
            [
                '45417360',
                new Money('10000.00', 'JPY'),
                new CountryCode('JP'),
                Money::createFromBaseCurrency('119.89'),
                Money::createFromBaseCurrency('1.66819')
            ],
            [
                '41417360',
                new Money('130.00', 'USD'),
                new CountryCode('US'),
                Money::createFromBaseCurrency('1.1213'),
                Money::createFromBaseCurrency('2.31873')
            ],
            [
                '4745030',
                new Money('2000.00', 'GBP'),
                new CountryCode('GB'),
                Money::createFromBaseCurrency('0.90328'),
                Money::createFromBaseCurrency('44.28305')
            ]
        ];
    }

    public function dataProviderForConvertToBaseCurrency(): array
    {
        return [
            [
                new Money('100.00', 'EUR'),
                Money::createFromBaseCurrency('1'),
                Money::createFromBaseCurrency('100.0000')
            ],
            [
                new Money('50.00', 'USD'),
                Money::createFromBaseCurrency('1.1213'),
                Money::createFromBaseCurrency('44.59109')
            ],
            [
                new Money('10000.00', 'JPY'),
                Money::createFromBaseCurrency('119.89'),
                Money::createFromBaseCurrency('83.40979')
            ],
            [
                new Money('130.00', 'USD'),
                Money::createFromBaseCurrency('1.1213'),
                Money::createFromBaseCurrency('115.93685')
            ],
            [
                new Money('2000.00', 'GBP'),
                Money::createFromBaseCurrency('0.90328'),
                Money::createFromBaseCurrency('2214.15286')
            ]
        ];
    }

    public function dataProviderForCalcCommissionPercent(): array
    {
        return [
            [
                '45717360',
                new CountryCode('DK'),
                '0.01'
            ],
            [
                '516793',
                new CountryCode('LT'),
                '0.01'
            ],
            [
                '45417360',
                new CountryCode('JP'),
                '0.02'
            ],
            [
                '41417360',
                new CountryCode('US'),
                '0.02'

            ],
            [
                '4745030',
                new CountryCode('GB'),
                '0.02'
            ]
        ];
    }

    /**
     * @dataProvider dataProviderForConvertToBaseCurrency
     */
    public function testConvertToBaseCurrency(Money $amount, Money $rate, Money $expectedResult)
    {
        $this->rateDataProviderMock->method('findRateToBaseCurrency')->willReturn($rate);
        $result = $this->invokeMethod($this->calculator, 'convertToBaseCurrency', [$amount]);
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @dataProvider dataProviderForCalcCommissionPercent
     */
    public function testCalcCommissionPercent(string $bin, CountryCode $code, string $expectedResult)
    {
        $this->binDataProviderMock->method('findCountryCode')->willReturn($code);
        $result = $this->invokeMethod($this->calculator, 'calcCommissionPercent', [$bin]);
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @dataProvider dataProviderForCalc
     */
    public function testCalc(string $bin, Money $transactionAmount, CountryCode $code, Money $rate, Money $expectedResult): void
    {
        $this->binDataProviderMock->method('findCountryCode')->willReturn($code);
        $this->rateDataProviderMock->method('findRateToBaseCurrency')->willReturn($rate);

        $transaction = new Transaction($bin, $transactionAmount, $this->calculator);
        $result = $this->calculator->calc($transaction);
        $this->assertEquals($expectedResult, $result);
    }
}