<?php

use PHPUnit\Framework\TestCase;
use App\Providers\Bin\CountryCode;
use App\Providers\Bin\BinlistDataProvider;
use App\Infrastructure\HttpClientInterface;
use App\Infrastructure\ServerNotAvailableException;
use App\Providers\Bin\CountryNotFoundException;
use App\Providers\Bin\BadResponseException;

class BinlistDataProviderTest extends TestCase
{

    public function testFindCountryCode(): void
    {
        $mock = $this->createMock(HttpClientInterface::class);

        $mock->method('send')
            ->willReturn([
                'number' => [
                    'length' => 16,
                    'luhn' => true
                ],
                'scheme' => 'visa',
                'type' => 'debit',
                'brand' => 'Visa/Dankort',
                'prepaid' => false,
                'country' => [
                    'numeric' => '208',
                    'alpha2' => 'DK',
                    'name' => 'Denmark',
                    'emoji' => '',
                    'currency' => 'DKK',
                    'latitude' => 56,
                    'longitude' => 10,
                    'bank' => [
                        'name' => 'Jyske Bank',
                        'url' => 'www.jyskebank.dk',
                        'phone' => '+4589893300',
                        'city' => 'Hjørring'
                    ]
                ]
            ]);

        $countryCode = new CountryCode('DK');

        $provider = new BinlistDataProvider($mock);
        $result = $provider->findCountryCode('45717360');
        $this->assertEquals($result, $countryCode);

        $mock->method('send')->willThrowException(new ServerNotAvailableException());
        $this->expectException(CountryNotFoundException::class);
        $provider->findCountryCode('45717360');

        $mock->method('send')->willReturn([]);
        $this->expectException(BadResponseException::class);
        $provider->findCountryCode('45717360');
    }
}