<?php

use App\Providers\Transaction\TransactionInterface;
use App\Providers\Transaction\FileTransactionDataProvider;

require __DIR__ . '/vendor/autoload.php';

if (empty($argv[1])) {
    exit;
}
$filename = __DIR__ . DIRECTORY_SEPARATOR . $argv[1];
$dataProvider = new FileTransactionDataProvider($filename);
$items = $dataProvider->getData();

foreach ($items as $item) {
    /** @var TransactionInterface $item*/
    $commission = $item->getCommission()->ceil(2);
    echo $commission->getAmount(), "\n";
}