<?php

namespace App\Common;

/**
 * Class to operate money data
 * @package App\Common
 */
class Money
{
    /** @var int number of digits after point */
    private const SCALE = 5;

    /** @var string symbols of base currency */
    private const BASE_CURRENCY = 'EUR';

    /** @var string the money amount */
    private $amount;

    /** @var string the money currency */
    private $currency;

    /**
     * Money constructor.
     *
     * @param string $amount the money amount
     * @param string $currency the money currency
     */
    public function __construct(string $amount, string $currency)
    {
        $this->amount = MathHelper::ceil($amount, static::SCALE);
        $this->currency = $currency;
    }

    /**
     * Get the money amount
     *
     * @return string the money amount
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * Get the money currency
     *
     * @return string the money currency
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * Convert the money to a defined currency
     *
     * @param string $coefficient coefficient for converting
     *
     * @return Money a new money object
     */
    public function convertToBaseCurrency(string $coefficient): Money
    {
        $value = $this->divide($coefficient);
        return new Money($value->getAmount(), static::BASE_CURRENCY);
    }

    /**
     * Divide the currency amount by defined coefficient
     *
     * @param string $coefficient the coefficient value
     * @return Money a new money object
     */
    public function divide(string $coefficient): Money
    {
        $value = bcdiv($this->getAmount(), $coefficient, static::SCALE);
        return new Money($value, $this->currency);
    }

    /**
     * Multiply the currency amount by defined coefficient
     *
     * @param string $coefficient the coefficient value
     * @return Money a new money object
     */
    public function multiply(string $coefficient): Money
    {
        $value = bcmul($this->getAmount(), $coefficient, static::SCALE);
        return new Money($value, $this->currency);
    }

    /**
     * Round the money value to ceil
     *
     * @param int $scale
     * @return Money
     */
    public function ceil(int $scale = 2): Money
    {
        $amount = MathHelper::ceil($this->amount, $scale);
        return new Money($amount, $this->currency);
    }

    /**
     * Check if the money has base currency
     *
     * @return bool status of checking
     */
    public function hasBaseCurrency(): bool
    {
        return $this->currency === static::BASE_CURRENCY;
    }

    /**
     * Create money with base currency
     *
     * @param string $amount the money amount
     * @return Money
     */
    public static function createFromBaseCurrency(string $amount)
    {
        return new Money($amount, static::BASE_CURRENCY);
    }

}