<?php

namespace App\Common;

/**
 * Helper to work with digits
 * @package App\Common
 */
class MathHelper
{
    /** @var string separator symbol */
    private const FRACTIONAL_SEPARATOR = '.';

    /**
     * Calc fractional digits amount
     *
     * @param string $value
     * @return int
     */
    private static function calcFractionalLength(string $value): int
    {
        $separatorPosition = strrpos($value, static::FRACTIONAL_SEPARATOR);
        if ($separatorPosition === false) {
            return 0;
        }
        $totalLength = strlen($value);
        return $totalLength - $separatorPosition;
    }

    /**
     * Round the value to ceil
     *
     * @param string $value
     * @param int $scale
     *
     * @return string
     */
    public static function ceil(string $value, int $scale = 2): string
    {
        $valueScale = static::calcFractionalLength($value);
        $fraction = bcmod(
            bcmul($value, bcpow(10, $scale), $valueScale),
            floor(bcmul($value, bcpow(10, $scale), $valueScale)),
            $valueScale
        );

        if ($fraction > 0) {
            $temp = bcdiv('1', bcpow(10, $scale), $scale);
            return bcadd($value, $temp , $scale);
        } else {
            return bcadd($value, 0, $scale);
        }
    }
}