<?php

namespace App\Infrastructure;

/**
 * Class ServerNotAvailableException
 * @package App\Infrastructure
 */
class ServerNotAvailableException extends ClientException
{

}