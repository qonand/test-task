<?php

namespace App\Infrastructure;

/**
 * Client to interact with services thought http
 * @package App\Infrastructure
 */
class HttpClient implements HttpClientInterface
{
    /** @var string URL to interact with the service */
    private $url;

    /**
     * HttpClient constructor.
     * @param string $url URL to interact with the service
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * Send GET request to defined endpoint
     *
     * @param string $endpoint the endpoint
     * @return array response data
     * @throws BadResponseException
     * @throws ServerNotAvailableException
     */
    public function send(string $endpoint = ''): array
    {
        $result = file_get_contents($this->url . $endpoint);
        if (!$result) {
            throw new ServerNotAvailableException('Server is not available');
        }

        $data = json_decode($result, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new BadResponseException('Response is not correct');
        }

        return $data;
    }

}