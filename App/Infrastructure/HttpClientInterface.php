<?php

namespace App\Infrastructure;

/**
 * Contract to interact with services thought http
 * @package App\Infrastructure
 */
interface HttpClientInterface
{
    /**
     * Send GET request to defined endpoint
     *
     * @param string $endpoint the endpoint
     * @return array response data
     * @throws BadResponseException
     * @throws ServerNotAvailableException
     */
    public function send(string $endpoint = ''): array;
}