<?php

namespace App\Infrastructure;

use Exception;

/**
 * Base class to all http client exception
 * @package App\Infrastructure
 */
class ClientException extends Exception
{

}