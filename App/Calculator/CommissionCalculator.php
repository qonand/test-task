<?php

namespace App\Calculator;

use App\Common\Money;
use App\Providers\Bin\BinDataProviderInterface;
use App\Providers\Rate\RateDataProviderInterface;
use App\Providers\Rate\RateException;
use App\Providers\Bin\BinDataException;
use App\Providers\Transaction\TransactionInterface;

/**
 * C;ass to calc commission by a transaction
 * @package App\Calculator
 */
class CommissionCalculator implements CommissionCalculatorInterface
{
    /**
     * @var string coefficient to exchange money from all countries except
     * countries which are locate in euro zone
     */
    private const COMMISSION_PERCENT = '0.02';

    /**
     * @var string coefficient to exchange money from all countries which
     * are locate in euro zone
     */
    private const EURO_ZONE_COMMISSION_PERCENT = '0.01';

    /** @var RateDataProviderInterface service to get rates by currencies */
    private $rateDataProvider;

    /** @var BinDataProviderInterface service to get a country by bin */
    private $binDataProvider;

    /**
     * CommissionCalculator constructor.
     * @param BinDataProviderInterface $binDataProvider service to get rates by currencies
     * @param RateDataProviderInterface $rateDataProvider service to get a country by bin
     */
    public function __construct(BinDataProviderInterface $binDataProvider, RateDataProviderInterface $rateDataProvider)
    {
        $this->binDataProvider = $binDataProvider;
        $this->rateDataProvider = $rateDataProvider;
    }

    /**
     * Convert money to the euro
     *
     * @param Money $value money that will be converted
     * @return Money
     * @throws ConvertException
     */
    protected function convertToBaseCurrency(Money $value): Money
    {
        if ($value->hasBaseCurrency()) {
            $rate = 1;
        } else {
            try {
                $money = $this->rateDataProvider->findRateToBaseCurrency($value->getCurrency());
            } catch (RateException $exception) {
                throw new ConvertException("Can't calculate rate");
            }
            $rate = $money->getAmount();
        }

        return $value->convertToBaseCurrency($rate);
    }

    /**
     * Calculate commission percent
     *
     * @param string $bin bin value
     * @return string commission percent
     * @throws CommissionException
     */
    protected function calcCommissionPercent(string $bin): string
    {
        try {
            $countryCode = $this->binDataProvider->findCountryCode($bin);
        } catch (BinDataException $exception) {
            throw new CommissionException("Can't calculate commission");
        }
        return $countryCode->isInEuroZone() ? static::EURO_ZONE_COMMISSION_PERCENT : static::COMMISSION_PERCENT;
    }

    /** @inheritdoc */
    public function calc(TransactionInterface $transaction): Money
    {
        $transactionEuroAmount = $this->convertToBaseCurrency($transaction->getValue());
        $percent = $this->calcCommissionPercent($transaction->getBin());
        return $transactionEuroAmount->multiply($percent);
    }
}