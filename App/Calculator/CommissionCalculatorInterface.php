<?php

namespace App\Calculator;

use App\Common\Money;
use App\Providers\Transaction\TransactionInterface;

/**
 * Contract to calc commission by a transaction
 * @package App\Calculator
 */
interface CommissionCalculatorInterface
{
    /**
     * Calc commission
     *
     * @param TransactionInterface $transaction the transaction to calc commission
     * @return Money commission amount
     * @throws ConvertException
     * @throws CommissionException
     */
    public function calc(TransactionInterface $transaction): Money;
}