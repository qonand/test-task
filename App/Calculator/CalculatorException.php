<?php

namespace App\Calculator;

use Exception;

/**
 * Base exception for calculator
 * @package App\Providers\Bin
 */
abstract class CalculatorException extends Exception
{

}