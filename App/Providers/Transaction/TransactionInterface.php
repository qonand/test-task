<?php

namespace App\Providers\Transaction;

use App\Common\Money;

/**
 * Contract to interact with transaction
 * @package App\Providers\Transaction
 */
interface TransactionInterface
{
    /**
     * Get bin number
     *
     * @return int bin number value
     */
    public function getBin(): int;

    /**
     * Get money amount
     *
     * @return Money money amount
     */
    public function getValue(): Money;

    /**
     * Get commission by the transaction
     *
     * @return Money
     */
    public function getCommission(): Money;
}