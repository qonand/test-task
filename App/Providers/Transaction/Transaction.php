<?php

namespace App\Providers\Transaction;

use App\Common\Money;
use App\Calculator\CommissionCalculatorInterface;

/**
 * Class to contain transaction data
 * @package App\Providers\Transaction
 */
class Transaction implements TransactionInterface
{
    /** @var int bin number */
    private $bin;

    /** @var Money the transaction amount */
    private $value;

    /** @var Money the commission amount */
    private $commission;

    /** @var CommissionCalculatorInterface service to calc commission by transaction */
    private $commissionCalculator;

    /**
     * InputData constructor.
     *
     * @param int $bin bin number
     * @param Money $amount money amount
     * @param CommissionCalculatorInterface $commissionCalculator service to calc commission by transaction
     */
    public function __construct(int $bin, Money $amount, CommissionCalculatorInterface $commissionCalculator)
    {
        $this->bin = $bin;
        $this->value = $amount;
        $this->commissionCalculator = $commissionCalculator;
    }

    /** @inheritDoc */
    public function getBin(): int
    {
        return $this->bin;
    }

    /** @inheritDoc */
    public function getValue(): Money
    {
        return $this->value;
    }

    /** @inheritDoc */
    public function getCommission(): Money
    {
        if (empty($this->commission)) {
            $this->commission = $this->commissionCalculator->calc($this);
        }
        return $this->commission;
    }

}