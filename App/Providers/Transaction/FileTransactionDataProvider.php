<?php

namespace App\Providers\Transaction;

use InvalidArgumentException;

/**
 * Data provider to get input data
 * @package App\Providers\Transaction
 */
class FileTransactionDataProvider implements TransactionDataProviderInterface
{

    /** @var string the file name that contains data */
    private $filename;

    /**
     * FileInputDataProvider constructor.
     *
     * @param string $filename the file name that contains data
     */
    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    /**
     * @inheritdoc
     */
    public function getData(): array
    {
        if (!is_file($this->filename) || !is_readable($this->filename)) {
            throw new InvalidArgumentException('The filename is incorrect or not readable');
        }

        $lines = file($this->filename);

        $data = [];
        foreach ($lines as $line) {
            $items = json_decode($line, true);
            ['bin' => $bin, 'amount' => $amount, 'currency' => $currency] = $items;
            if (empty($bin) || empty($amount) || empty($currency)) {
                throw new IncorrectTransactionDataException('Incorrect data in the input file: ' . $line);
            }
            $data[] = TransactionFactory::build($bin, $amount, $currency);
        }

        return $data;
    }
}