<?php

namespace App\Providers\Transaction;

use Exception;

/**
 * Exception to notice about incorrect input data
 * @package App\Providers\Transaction
 */
class IncorrectTransactionDataException extends Exception
{

}