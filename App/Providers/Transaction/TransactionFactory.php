<?php

namespace App\Providers\Transaction;

use App\Common\Money;
use App\Calculator\CommissionCalculator;
use App\Infrastructure\HttpClient;
use App\Providers\Bin\BinlistDataProvider;
use App\Providers\Rate\ExchangeRatesApiDataProvider;

/**
 * Factory to build the transaction model
 * @package App\Providers\Transaction
 */
class TransactionFactory
{
    /**
     * Factory-method to build an object with input data
     *
     * @param int $bin bin number
     * @param string $amount money amount
     * @param string $currency money currency code
     *
     * @return TransactionInterface the object with input data
     */
    public static function build(int $bin, string $amount, string $currency): TransactionInterface
    {
        $amountWithCurrency = new Money($amount, $currency);
        $binListClient = new HttpClient('https://lookup.binlist.net/');
        $binDataProvider = new BinlistDataProvider($binListClient);
        $exchangeRateApiUrl = new HttpClient('https://api.exchangeratesapi.io/latest');
        $rateDataProvider = new ExchangeRatesApiDataProvider($exchangeRateApiUrl);
        $calculator = new CommissionCalculator($binDataProvider, $rateDataProvider);
        return new Transaction($bin, $amountWithCurrency, $calculator);
    }
}