<?php

namespace App\Providers\Transaction;

/**
 * Contact to interact with input provider
 * @package App\Providers\Transaction
 */
interface TransactionDataProviderInterface
{
    /**
     * Get all input data
     *
     * @return TransactionInterface[] list of input data
     * @throws IncorrectTransactionDataException
     */
    public function getData(): array;
}