<?php

namespace App\Providers\Bin;

/**
 * Class CountryNotFoundException
 * @package App\Providers\Bin
 */
class CountryNotFoundException extends BinDataException
{

}