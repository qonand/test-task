<?php

namespace App\Providers\Bin;

/**
 * Contact to interact with bin provider
 * @package App\Providers\Bin
 */
interface BinDataProviderInterface
{
    /**
     * Find county code by bin value
     *
     * @param string $bin bin value
     * @return CountryCode country code object
     * @throws CountryNotFoundException
     * @throws BadResponseException
     */
    public function findCountryCode(string $bin): CountryCode;
}