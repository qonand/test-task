<?php

namespace App\Providers\Bin;

use Exception;

/**
 * Base exception for bin data provider
 * @package App\Providers\Bin
 */
abstract class BinDataException extends Exception
{

}