<?php

namespace App\Providers\Bin;

/**
 * Class to operate country code
 * @package App\Providers\Bin
 */
class CountryCode
{
    /** @var string the code value */
    private $value;

    /** @var string[] list of code values which is located in euro zone */
    private const EURO_ZONE_COUNTRY_CODES = [
        'AT',
        'BE',
        'BG',
        'CY',
        'CZ',
        'DE',
        'DK',
        'EE',
        'ES',
        'FI',
        'FR',
        'GR',
        'HR',
        'HU',
        'IE',
        'IT',
        'LT',
        'LU',
        'LV',
        'MT',
        'NL',
        'PO',
        'PT',
        'RO',
        'SE',
        'SI',
        'SK'
    ];

    /**
     * CountryCode constructor.
     *
     * @param string $value the code value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Get the code value
     *
     * @return string the code value
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * Check if the country is located in euro zone
     *
     * @return bool status of checking
     */
    public function isInEuroZone(): bool
    {
        return in_array($this->value, static::EURO_ZONE_COUNTRY_CODES);
    }
}