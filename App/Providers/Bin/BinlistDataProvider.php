<?php

namespace App\Providers\Bin;

use App\Infrastructure\ClientException;
use App\Infrastructure\HttpClientInterface;

/**
 * Data provider to get country code by bin through lookup.binlist.net
 * @package App\Providers\Bin
 */
class BinlistDataProvider implements BinDataProviderInterface
{
    /** @var HttpClientInterface client to interact with HTTP */
    private $httpClient;

    /**
     * BinlistDataProvider constructor.
     * @param HttpClientInterface $client client to interact with HTTP
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->httpClient = $client;
    }

    /** @inheritdoc */
    public function findCountryCode(string $bin): CountryCode
    {
        try {
            $data = $this->httpClient->send($bin);
        } catch (ClientException $exception) {
            throw new CountryNotFoundException('Country with bin value ' . $bin . ' not found');
        }

        if (!isset($data['country']['alpha2'])) {
            throw new BadResponseException('Bad response from lookup.binlist.net');
        }
        return new CountryCode($data['country']['alpha2']);
    }
}