<?php

namespace App\Providers\Rate;

/**
 * Class CurrencyNotFoundException
 * @package App\Providers\Rate
 */
class CurrencyNotFoundException extends RateException
{

}