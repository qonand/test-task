<?php

namespace App\Providers\Rate;

use App\Common\Money;

/**
 * Contact to interact with rate data provider
 * @package App\Providers\Rate
 */
interface RateDataProviderInterface
{
    /**
     * Find rate to base currency
     *
     * @param string $currency the currency that will be converted to euro
     *
     * @return Money the rate
     * @throws BadResponseException
     * @throws CurrencyNotFoundException
     */
    public function findRateToBaseCurrency(string $currency): Money;

}