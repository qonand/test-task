<?php

namespace App\Providers\Rate;

use Exception;

/**
 * Class RateException
 * @package App\Providers\Rate
 */
abstract class RateException extends Exception
{

}