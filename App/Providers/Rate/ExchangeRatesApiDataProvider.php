<?php

namespace App\Providers\Rate;

use App\Common\Money;
use App\Infrastructure\ClientException;
use App\Infrastructure\HttpClientInterface;

/**
 * Data provider to get country code by bin through exchangeratesapi.io
 * @package App\Providers\Rate
 */
class ExchangeRatesApiDataProvider implements RateDataProviderInterface
{

    /** @var HttpClientInterface client to interact with HTTP */
    private $httpClient;

    /**
     * BinlistDataProvider constructor.
     * @param HttpClientInterface $client client to interact with HTTP
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->httpClient = $client;
    }

    /** @inheritdoc */
    public function findRateToBaseCurrency(string $currency): Money
    {
        try {
            $data = $this->httpClient->send();
        } catch (ClientException $exception) {
            throw new BadResponseException('Invalid response from server');
        }

        if (!isset($data['rates'])) {
            throw new BadResponseException('Invalid response from server');
        }

        if (!isset($data['rates'][$currency])) {
            throw new CurrencyNotFoundException('Currency '. $currency . ' not found');
        }

        return Money::createFromBaseCurrency($data['rates'][$currency]);
    }

}